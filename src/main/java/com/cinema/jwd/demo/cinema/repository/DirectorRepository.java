package com.cinema.jwd.demo.cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cinema.jwd.demo.cinema.model.Director;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Integer>{

}
