package com.cinema.jwd.demo.cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cinema.jwd.demo.cinema.service.DirectorService;
import com.cinema.jwd.demo.cinema.web.dto.req.DirectorDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.DirectorResponseDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.SimpleBooleanResponseDTO;

@RestController
@RequestMapping("/director")
public class DirectorController{
	
	@Autowired
	DirectorService directorService;
	
	@PostMapping("create")
	public ResponseEntity<SimpleBooleanResponseDTO> create(@RequestBody DirectorDTO request){
		boolean response = false;
		response = directorService.create(request);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@PutMapping("update")
	public ResponseEntity<SimpleBooleanResponseDTO> update(@RequestBody DirectorDTO request, @RequestParam("directorId") Integer directorId){
		boolean response = false;
		response = directorService.update(request, directorId);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@DeleteMapping("delete")
	public ResponseEntity<SimpleBooleanResponseDTO> delete(@RequestParam("directorId") Integer directorId){
		boolean response = false;
		response = directorService.delete(directorId);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@GetMapping("findAll")
	public ResponseEntity<List<DirectorResponseDTO>> findAll(){
		List<DirectorResponseDTO> response = directorService.findAll();
		return new ResponseEntity<List<DirectorResponseDTO>>(response, HttpStatus.OK);
	}
	
	@GetMapping("findById")
	public ResponseEntity<DirectorResponseDTO> findById(@RequestParam("directorId") Integer directorId){
		DirectorResponseDTO response = directorService.findById(directorId);
		return new ResponseEntity<DirectorResponseDTO>(response, HttpStatus.OK);
	}
}

