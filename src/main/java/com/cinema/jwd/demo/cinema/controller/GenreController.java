package com.cinema.jwd.demo.cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cinema.jwd.demo.cinema.service.GenreService;
import com.cinema.jwd.demo.cinema.web.dto.req.DirectorDTO;
import com.cinema.jwd.demo.cinema.web.dto.req.GenreDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.DirectorResponseDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.GenreResponseDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.SimpleBooleanResponseDTO;

@RestController
@RequestMapping("/genre")
public class GenreController{
	@Autowired
	GenreService genreService;
	
	@PostMapping("create")
	public ResponseEntity<SimpleBooleanResponseDTO> create(@RequestBody GenreDTO request){
		boolean response = false;
		response = genreService.create(request);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@PutMapping("update")
	public ResponseEntity<SimpleBooleanResponseDTO> update(@RequestBody GenreDTO request, @RequestParam("genreId") Integer genreId){
		boolean response = false;
		response = genreService.update(request, genreId);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@DeleteMapping("delete")
	public ResponseEntity<SimpleBooleanResponseDTO> delete(@RequestParam("genreId") Integer genreId){
		boolean response = false;
		response = genreService.delete(genreId);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@GetMapping("findAll")
	public ResponseEntity<List<GenreResponseDTO>> findAll(){
		List<GenreResponseDTO> response = genreService.findAll();
		return new ResponseEntity<List<GenreResponseDTO>>(response, HttpStatus.OK);
	}
	
	@GetMapping("findById")
	public ResponseEntity<GenreResponseDTO> findById(@RequestParam("genreId") Integer genreId){
		GenreResponseDTO response = genreService.findById(genreId);
		return new ResponseEntity<GenreResponseDTO>(response, HttpStatus.OK);
	}
}