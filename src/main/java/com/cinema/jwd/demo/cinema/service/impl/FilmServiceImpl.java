package com.cinema.jwd.demo.cinema.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cinema.jwd.demo.cinema.model.Actor;
import com.cinema.jwd.demo.cinema.model.Director;
import com.cinema.jwd.demo.cinema.model.Film;
import com.cinema.jwd.demo.cinema.model.FilmActor;
import com.cinema.jwd.demo.cinema.model.Genre;
import com.cinema.jwd.demo.cinema.repository.ActorRepository;
import com.cinema.jwd.demo.cinema.repository.DirectorRepository;
import com.cinema.jwd.demo.cinema.repository.FilmActorRepository;
import com.cinema.jwd.demo.cinema.repository.FilmRepository;
import com.cinema.jwd.demo.cinema.repository.GenreRepository;
import com.cinema.jwd.demo.cinema.service.ActorService;
import com.cinema.jwd.demo.cinema.service.DirectorService;
import com.cinema.jwd.demo.cinema.service.FilmService;
import com.cinema.jwd.demo.cinema.service.GenreService;
import com.cinema.jwd.demo.cinema.web.dto.req.FilmDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.ActorResponseDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.FilmResponseDTO;

@Service
public class FilmServiceImpl implements FilmService{
	
	@Autowired
	FilmRepository filmRepository;
	
	@Autowired
	ActorRepository actorRepository;
	
	@Autowired
	DirectorRepository directorRepository;
	
	@Autowired
	GenreRepository genreRepository;
	
	@Autowired
	FilmActorRepository filmActorRepository;
	
	@Autowired
	GenreService genreService;
	
	@Autowired
	DirectorService directorService;
	
	@Autowired
	ActorService actorService;

	@Override
	public boolean create(FilmDTO request) {
		List<Actor> actorsForFilm = new ArrayList<Actor>();
		for(Integer actorId:request.getActorIds()) {
			Optional<Actor> actorOpt = actorRepository.findById(actorId);
			if(actorOpt.isEmpty()) {
				return false;
			}
			Actor actor = actorOpt.get();
			actorsForFilm.add(actor);
		}
		
		Optional<Director> directorOpt = directorRepository.findById(request.getDirectorId());
		if(directorOpt.isEmpty()) {
			return false;
		}
		Director director = directorOpt.get();
		
		Optional<Genre> genreOpt = genreRepository.findById(request.getGenreId());
		if(genreOpt.isEmpty()) {
			return false;
		}
		Genre genre = genreOpt.get();
		
		Film newFilm = new Film();
		newFilm.setDirector(director);
		newFilm.setGenre(genre);
		newFilm.setDescription(request.getDescription());
		newFilm.setTitle(request.getTitle());
		newFilm.setYear(request.getYear());
		
		newFilm = filmRepository.saveAndFlush(newFilm);
		
		for(Actor actor:actorsForFilm) {
			FilmActor filmActorTmp = new FilmActor();
			filmActorTmp.setFilm(newFilm);
			filmActorTmp.setActor(actor);
			filmActorRepository.save(filmActorTmp);
		}
		
		return true;
	}

	@Override
	public boolean update(FilmDTO request, Integer filmId) {
		List<Actor> actorsForFilm = new ArrayList<Actor>();
		for(Integer actorId:request.getActorIds()) {
			Optional<Actor> actorOpt = actorRepository.findById(actorId);
			if(actorOpt.isEmpty()) {
				return false;
			}
			Actor actor = actorOpt.get();
			actorsForFilm.add(actor);
		}
		
		Optional<Director> directorOpt = directorRepository.findById(request.getDirectorId());
		if(directorOpt.isEmpty()) {
			return false;
		}
		Director director = directorOpt.get();
		
		Optional<Genre> genreOpt = genreRepository.findById(request.getGenreId());
		if(genreOpt.isEmpty()) {
			return false;
		}
		Genre genre = genreOpt.get();
		
		Optional<Film> filmOpt = filmRepository.findById(filmId);
		if(filmOpt.isEmpty()) {
			return false;
		}
		Film filmForUpdate = filmOpt.get();
		filmForUpdate.setDirector(director);
		filmForUpdate.setGenre(genre);
		filmForUpdate.setDescription(request.getDescription());
		filmForUpdate.setTitle(request.getTitle());
		filmForUpdate.setYear(request.getYear());
		
		filmRepository.save(filmForUpdate);
		
		//add new actors if its needed
		for(Actor actor:actorsForFilm) {
			boolean existingActor = false;
			for(FilmActor filmActor:filmForUpdate.getFilmActors()) {
				if(filmActor.getActor().getActorId() == actor.getActorId()) {
					existingActor=true;
					break;
				}
			}
			if(!existingActor) {
				FilmActor filmActorTmp = new FilmActor();
				filmActorTmp.setFilm(filmForUpdate);
				filmActorTmp.setActor(actor);
				filmActorRepository.save(filmActorTmp);
			}
		}
		
		//remove old actors if its needed
		for(FilmActor filmActor:filmForUpdate.getFilmActors()) {
			boolean existingActor = false;
			for(Actor actor:actorsForFilm) {
				if(filmActor.getActor().getActorId() == actor.getActorId()) {
					existingActor=true;
					break;
				}
			}
			if(!existingActor) {
				filmActorRepository.delete(filmActor);
			}
		}
		
		return true;
	}

	@Override
	public boolean delete(Integer filmId) {
		try {
			filmRepository.deleteById(filmId);
		}catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<FilmResponseDTO> findAll() {
		List<FilmResponseDTO> response = new ArrayList<FilmResponseDTO>();
		List<Film> films = filmRepository.findAll();
		for (Film film : films) {
			FilmResponseDTO tmpFilm = new FilmResponseDTO();
			tmpFilm.setFilmId(film.getFilmId());
			tmpFilm.setDirector(directorService.findById(film.getDirector().getDirectorId()));
			tmpFilm.setGenre(genreService.findById(film.getGenre().getGenreId()));
			tmpFilm.setDescription(film.getDescription());
			tmpFilm.setTitle(film.getTitle());
			tmpFilm.setYear(film.getYear());
			List<ActorResponseDTO> actors = new ArrayList<ActorResponseDTO>();
			for(FilmActor filmActor:film.getFilmActors()) {
				ActorResponseDTO tmpActor = actorService.findById(filmActor.getActor().getActorId());
				actors.add(tmpActor);
			}
			tmpFilm.setActors(actors);
			response.add(tmpFilm);
		}
		return response;
	}

	@Override
	public FilmResponseDTO findById(Integer filmId) {
		Optional<Film> filmOpt = filmRepository.findById(filmId);
		if(filmOpt.isEmpty()) {
			return null;
		}
		Film film = filmOpt.get();
		FilmResponseDTO response = new FilmResponseDTO();
		response.setFilmId(film.getFilmId());
		response.setDirector(directorService.findById(film.getDirector().getDirectorId()));
		response.setGenre(genreService.findById(film.getGenre().getGenreId()));
		response.setDescription(film.getDescription());
		response.setTitle(film.getTitle());
		response.setYear(film.getYear());
		List<ActorResponseDTO> actors = new ArrayList<ActorResponseDTO>();
		for(FilmActor filmActor:film.getFilmActors()) {
			ActorResponseDTO tmpActor = actorService.findById(filmActor.getActor().getActorId());
			actors.add(tmpActor);
		}
		response.setActors(actors);
		return response;
	}

}
