package com.cinema.jwd.demo.cinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {
	    org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
	})
public class TestMavenCinemaApplication extends SpringBootServletInitializer {
	 @Override
     protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		 return application.sources(TestMavenCinemaApplication.class);
     }
	
	 public static void main(String[] args) {
		SpringApplication.run(TestMavenCinemaApplication.class, args);
	}
}
