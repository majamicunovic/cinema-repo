package com.cinema.jwd.demo.cinema.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cinema.jwd.demo.cinema.model.Actor;
import com.cinema.jwd.demo.cinema.repository.ActorRepository;
import com.cinema.jwd.demo.cinema.service.ActorService;
import com.cinema.jwd.demo.cinema.web.dto.req.ActorDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.ActorResponseDTO;

@Service
public class ActorServiceImpl implements ActorService{
	
	@Autowired
	ActorRepository actorRepository;

	@Override
	public boolean create(ActorDTO request) {
		Actor newActor = new Actor();
		newActor.setName(request.getName());
		newActor.setSurname(request.getSurname());
		actorRepository.save(newActor);
		return true;
	}

	@Override
	public boolean update(ActorDTO request, Integer actorId) {
		Optional<Actor> actorOpt = actorRepository.findById(actorId);
		if(actorOpt.isEmpty()) {
			return false;
		}
		Actor actorForUpdate = actorOpt.get();
		actorForUpdate.setName(request.getName());
		actorForUpdate.setSurname(request.getSurname());
		actorRepository.save(actorForUpdate);
		return true;
	}

	@Override
	public boolean delete(Integer actorId) {
		try {
			actorRepository.deleteById(actorId);
		}catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<ActorResponseDTO> findAll() {
		List<ActorResponseDTO> response = new ArrayList<ActorResponseDTO>();
		List<Actor> actors = actorRepository.findAll();
		for (Actor actor : actors) {
			ActorResponseDTO tmpActor = new ActorResponseDTO();
			tmpActor.setName(actor.getName());
			tmpActor.setSurname(actor.getSurname());
			tmpActor.setActorId(actor.getActorId());
			response.add(tmpActor);
		}
		return response;
	}

	@Override
	public ActorResponseDTO findById(Integer actorId) {
		Optional<Actor> actorOpt = actorRepository.findById(actorId);
		if(actorOpt.isEmpty()) {
			return null;
		}
		Actor actor = actorOpt.get();
		ActorResponseDTO tmpActor = new ActorResponseDTO();
		tmpActor.setName(actor.getName());
		tmpActor.setSurname(actor.getSurname());
		tmpActor.setActorId(actor.getActorId());
		return tmpActor;
	}

}
