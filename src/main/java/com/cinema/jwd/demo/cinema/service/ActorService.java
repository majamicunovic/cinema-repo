package com.cinema.jwd.demo.cinema.service;

import java.util.List;

import com.cinema.jwd.demo.cinema.web.dto.req.ActorDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.ActorResponseDTO;

public interface ActorService {

	boolean create(ActorDTO request);

	boolean update(ActorDTO request, Integer actorId);

	boolean delete(Integer actorId);

	List<ActorResponseDTO> findAll();

	ActorResponseDTO findById(Integer actorId);

}
