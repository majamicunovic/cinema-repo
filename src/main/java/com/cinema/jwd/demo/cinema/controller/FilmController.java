package com.cinema.jwd.demo.cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cinema.jwd.demo.cinema.service.ActorService;
import com.cinema.jwd.demo.cinema.service.FilmService;
import com.cinema.jwd.demo.cinema.web.dto.req.ActorDTO;
import com.cinema.jwd.demo.cinema.web.dto.req.FilmDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.ActorResponseDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.FilmResponseDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.SimpleBooleanResponseDTO;

@RestController
@RequestMapping("/film")
public class FilmController{
	@Autowired
	FilmService filmService;
	
	
	@PostMapping("create")
	public ResponseEntity<SimpleBooleanResponseDTO> create(@RequestBody FilmDTO request){
		boolean response = false;
		response = filmService.create(request);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@PutMapping("update")
	public ResponseEntity<SimpleBooleanResponseDTO> update(@RequestBody FilmDTO request, @RequestParam("filmId") Integer filmId){
		boolean response = false;
		response = filmService.update(request, filmId);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@DeleteMapping("delete")
	public ResponseEntity<SimpleBooleanResponseDTO> delete(@RequestParam("filmId") Integer filmId){
		boolean response = false;
		response = filmService.delete(filmId);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@GetMapping("findAll")
	public ResponseEntity<List<FilmResponseDTO>> findAll(){
		List<FilmResponseDTO> response = filmService.findAll();
		return new ResponseEntity<List<FilmResponseDTO>>(response, HttpStatus.OK);
	}
	
	@GetMapping("findById")
	public ResponseEntity<FilmResponseDTO> findById(@RequestParam("filmId") Integer filmId){
		FilmResponseDTO response = filmService.findById(filmId);
		return new ResponseEntity<FilmResponseDTO>(response, HttpStatus.OK);
	}
}
        


