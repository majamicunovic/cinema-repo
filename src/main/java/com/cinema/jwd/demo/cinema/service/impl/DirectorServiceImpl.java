package com.cinema.jwd.demo.cinema.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cinema.jwd.demo.cinema.model.Director;
import com.cinema.jwd.demo.cinema.repository.DirectorRepository;
import com.cinema.jwd.demo.cinema.service.DirectorService;
import com.cinema.jwd.demo.cinema.web.dto.req.DirectorDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.DirectorResponseDTO;

@Service
public class DirectorServiceImpl implements DirectorService{

	@Autowired
	DirectorRepository direcorRepository;
	
	@Override
	public boolean create(DirectorDTO request) {
		Director newDirector = new Director();
		newDirector.setName(request.getName());
		newDirector.setSurname(request.getSurname());
		direcorRepository.save(newDirector);
		return true;
	}

	@Override
	public boolean update(DirectorDTO request, Integer directorId) {
		Optional<Director> directorOpt = direcorRepository.findById(directorId);
		if(directorOpt.isEmpty()) {
			return false;
		}
		Director directorForUpdate = directorOpt.get();
		directorForUpdate.setName(request.getName());
		directorForUpdate.setSurname(request.getSurname());
		direcorRepository.save(directorForUpdate);
		return true;
	}

	@Override
	public boolean delete(Integer directorId) {
		try {
			direcorRepository.deleteById(directorId);
		}catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<DirectorResponseDTO> findAll() {
		List<DirectorResponseDTO> response = new ArrayList<DirectorResponseDTO>();
		List<Director> directors = direcorRepository.findAll();
		for (Director director : directors) {
			DirectorResponseDTO tmpDirector = new DirectorResponseDTO();
			tmpDirector.setName(director.getName());
			tmpDirector.setSurname(director.getSurname());
			tmpDirector.setDirectorId(director.getDirectorId());
			response.add(tmpDirector);
		}
		return response;
	}

	@Override
	public DirectorResponseDTO findById(Integer directorId) {
		Optional<Director> directorOpt = direcorRepository.findById(directorId);
		if(directorOpt.isEmpty()) {
			return null;
		}
		Director director = directorOpt.get();
		DirectorResponseDTO tmpDirector = new DirectorResponseDTO();
		tmpDirector.setName(director.getName());
		tmpDirector.setSurname(director.getSurname());
		tmpDirector.setDirectorId(director.getDirectorId());
		return tmpDirector;
	}

}
