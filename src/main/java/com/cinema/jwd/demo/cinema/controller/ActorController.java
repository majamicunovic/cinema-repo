package com.cinema.jwd.demo.cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cinema.jwd.demo.cinema.service.ActorService;
import com.cinema.jwd.demo.cinema.web.dto.req.ActorDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.ActorResponseDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.SimpleBooleanResponseDTO;

@RestController
@RequestMapping("/actor")
public class ActorController{
	
	@Autowired
	ActorService actorService;
	
	
	@PostMapping("create")
	public ResponseEntity<SimpleBooleanResponseDTO> create(@RequestBody ActorDTO request){
		boolean response = false;
		response = actorService.create(request);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@PutMapping("update")
	public ResponseEntity<SimpleBooleanResponseDTO> update(@RequestBody ActorDTO request, @RequestParam("actorId") Integer actorId){
		boolean response = false;
		response = actorService.update(request, actorId);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@DeleteMapping("delete")
	public ResponseEntity<SimpleBooleanResponseDTO> delete(@RequestParam("actorId") Integer actorId){
		boolean response = false;
		response = actorService.delete(actorId);
		return new ResponseEntity<SimpleBooleanResponseDTO>(new SimpleBooleanResponseDTO(response), HttpStatus.OK);
	}
	
	@GetMapping("findAll")
	public ResponseEntity<List<ActorResponseDTO>> findAll(){
		List<ActorResponseDTO> response = actorService.findAll();
		return new ResponseEntity<List<ActorResponseDTO>>(response, HttpStatus.OK);
	}
	
	@GetMapping("findById")
	public ResponseEntity<ActorResponseDTO> findById(@RequestParam("actorId") Integer actorId){
		ActorResponseDTO response = actorService.findById(actorId);
		return new ResponseEntity<ActorResponseDTO>(response, HttpStatus.OK);
	}
}