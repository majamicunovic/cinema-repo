package com.cinema.jwd.demo.cinema.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the director database table.
 * 
 */
@Entity
@NamedQuery(name="Director.findAll", query="SELECT d FROM Director d")
public class Director implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="director_id")
	private int directorId;

	private String name;

	private String surname;

	//bi-directional many-to-one association to Film
	@OneToMany(mappedBy="director")
	private List<Film> films;

	public Director() {
	}

	public int getDirectorId() {
		return this.directorId;
	}

	public void setDirectorId(int directorId) {
		this.directorId = directorId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<Film> getFilms() {
		return this.films;
	}

	public void setFilms(List<Film> films) {
		this.films = films;
	}

	public Film addFilm(Film film) {
		getFilms().add(film);
		film.setDirector(this);

		return film;
	}

	public Film removeFilm(Film film) {
		getFilms().remove(film);
		film.setDirector(null);

		return film;
	}

}