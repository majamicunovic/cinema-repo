package com.cinema.jwd.demo.cinema.web.dto.res;

public class GenreResponseDTO {
	private int genreId;

	private String name;

	public int getGenreId() {
		return genreId;
	}

	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
