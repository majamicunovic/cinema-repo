package com.cinema.jwd.demo.cinema.service;

import java.util.List;

import com.cinema.jwd.demo.cinema.web.dto.req.GenreDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.GenreResponseDTO;

public interface GenreService {

	boolean create(GenreDTO request);

	boolean update(GenreDTO request, Integer genreId);

	boolean delete(Integer genreId);

	List<GenreResponseDTO> findAll();

	GenreResponseDTO findById(Integer genreId);

}
