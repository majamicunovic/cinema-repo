package com.cinema.jwd.demo.cinema.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cinema.jwd.demo.cinema.model.Genre;
import com.cinema.jwd.demo.cinema.repository.GenreRepository;
import com.cinema.jwd.demo.cinema.service.GenreService;
import com.cinema.jwd.demo.cinema.web.dto.req.GenreDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.GenreResponseDTO;

@Service
public class GenreServiceImpl implements GenreService{
	
	@Autowired
	GenreRepository genreRepository;

	@Override
	public boolean create(GenreDTO request) {
		Genre newGenre = new Genre();
		newGenre.setName(request.getName());
		genreRepository.save(newGenre);
		return true;
	}

	@Override
	public boolean update(GenreDTO request, Integer genreId) {
		Optional<Genre> genreOpt = genreRepository.findById(genreId);
		if(genreOpt.isEmpty()) {
			return false;
		}
		Genre genreForUpdate = genreOpt.get();
		genreForUpdate.setName(request.getName());
		genreRepository.save(genreForUpdate);
		return true;
	}

	@Override
	public boolean delete(Integer genreId) {
		try {
			genreRepository.deleteById(genreId);
		}catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<GenreResponseDTO> findAll() {
		List<GenreResponseDTO> response = new ArrayList<GenreResponseDTO>();
		List<Genre> genres = genreRepository.findAll();
		for (Genre genre : genres) {
			GenreResponseDTO tmpGenre = new GenreResponseDTO();
			tmpGenre.setName(genre.getName());
			tmpGenre.setGenreId(genre.getGenreId());
			response.add(tmpGenre);
		}
		return response;
	}

	@Override
	public GenreResponseDTO findById(Integer genreId) {
		Optional<Genre> genreOpt = genreRepository.findById(genreId);
		if(genreOpt.isEmpty()) {
			return null;
		}
		Genre genre = genreOpt.get();
		GenreResponseDTO tmpGenre = new GenreResponseDTO();
		tmpGenre.setName(genre.getName());
		tmpGenre.setGenreId(genre.getGenreId());
		return tmpGenre;
	}

}
