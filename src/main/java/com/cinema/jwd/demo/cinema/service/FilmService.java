package com.cinema.jwd.demo.cinema.service;

import java.util.List;

import com.cinema.jwd.demo.cinema.web.dto.req.FilmDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.FilmResponseDTO;

public interface FilmService {

	boolean create(FilmDTO request);

	boolean update(FilmDTO request, Integer filmId);

	boolean delete(Integer filmId);

	List<FilmResponseDTO> findAll();

	FilmResponseDTO findById(Integer filmId);

}
