package com.cinema.jwd.demo.cinema.web.dto.res;

public class SimpleBooleanResponseDTO {
	private boolean status;

	
	
	public SimpleBooleanResponseDTO(boolean status) {
		super();
		this.status = status;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
}
