package com.cinema.jwd.demo.cinema.web.dto.req;

import java.util.List;

public class FilmDTO {
	private String description;
	private String title;
	private int year;
	private int directorId;
	private int genreId;
	private List<Integer> actorIds;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getDirectorId() {
		return directorId;
	}
	public void setDirectorId(int directorId) {
		this.directorId = directorId;
	}
	public int getGenreId() {
		return genreId;
	}
	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}
	public List<Integer> getActorIds() {
		return actorIds;
	}
	public void setActorIds(List<Integer> actorIds) {
		this.actorIds = actorIds;
	}
	
	
}
