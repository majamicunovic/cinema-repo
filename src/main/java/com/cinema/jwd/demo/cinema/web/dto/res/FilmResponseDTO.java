package com.cinema.jwd.demo.cinema.web.dto.res;

import java.util.List;

public class FilmResponseDTO {
	private int filmId;
	private String description;
	private String title;
	private int year;
	private DirectorResponseDTO director;
	private GenreResponseDTO genre;
	private List<ActorResponseDTO> actors;
	public int getFilmId() {
		return filmId;
	}
	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public DirectorResponseDTO getDirector() {
		return director;
	}
	public void setDirector(DirectorResponseDTO director) {
		this.director = director;
	}
	public GenreResponseDTO getGenre() {
		return genre;
	}
	public void setGenre(GenreResponseDTO genre) {
		this.genre = genre;
	}
	public List<ActorResponseDTO> getActors() {
		return actors;
	}
	public void setActors(List<ActorResponseDTO> actors) {
		this.actors = actors;
	}
	
	
}
