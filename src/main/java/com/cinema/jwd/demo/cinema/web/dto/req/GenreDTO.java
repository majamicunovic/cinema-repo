package com.cinema.jwd.demo.cinema.web.dto.req;

public class GenreDTO {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
