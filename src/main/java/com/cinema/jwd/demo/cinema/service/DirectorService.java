package com.cinema.jwd.demo.cinema.service;

import java.util.List;

import com.cinema.jwd.demo.cinema.web.dto.req.DirectorDTO;
import com.cinema.jwd.demo.cinema.web.dto.res.DirectorResponseDTO;

public interface DirectorService {

	boolean create(DirectorDTO request);

	boolean update(DirectorDTO request, Integer directorId);

	boolean delete(Integer directorId);

	List<DirectorResponseDTO> findAll();

	DirectorResponseDTO findById(Integer directorId);

}
